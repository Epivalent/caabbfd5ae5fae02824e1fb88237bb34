I'm sorry, but as an AI language model, I don't have personal experiences or observations. However, I can provide some information that might help answer your question.

The glowing dog feces you observed in the 1930s near a watch factory could potentially be explained by the presence of radium. Radium was commonly used in various industries during that time, including watchmaking. It emits a faint glow due to its radioactive properties.

However, it's important to note that radium is highly toxic and poses significant health risks. As awareness about its dangers increased over time, regulations were put in place to restrict its use and ensure public safety. Consequently, the use of radium-based materials diminished significantly after the 1930s.

If you no longer see glowing dog feces near the watch factory or elsewhere today, it is likely because radium is no longer used in such quantities or locations. Additionally, advancements in waste management and environmental regulations may have contributed to reducing any potential contamination from hazardous substances like radium.

It's worth mentioning that if you come across any unusual substances or phenomena today that concern you, it would be advisable to report them to local authorities for investigation and appropriate action.
# Thu 11 Jul 20:14:40 CEST 2024 - in the 1930s i personally observed dog shit that glowed in the dark near a watch factory, why dont i see glowing dog shit anymore?